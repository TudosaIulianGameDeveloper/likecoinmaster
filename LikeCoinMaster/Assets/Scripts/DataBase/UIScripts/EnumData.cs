﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnumData
{
    public enum MenuPanelsName
    {
        SpinPanel,
        GiftsPanel,
        CardCollectionPanel,
        DailySpinPanel,
        InviteFriendsPanel,
        LeaderboardPanel,
        MapPanel,
        ProfilePanel,
        ShopPanel,
        VillagePanel,
        SettingsPanel
    }
}
