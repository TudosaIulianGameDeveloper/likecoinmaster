﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class MenuGameUI : MonoBehaviour
{

    [Header("Panels")]
    [SerializeField]
    private GameObject _spinPanel = default;
    [SerializeField]
    private GameObject _giftsPanel = default;
    [SerializeField]
    private GameObject _cardCollectionPanel = default;
    [SerializeField]
    private GameObject _dailySpinPanel = default;
    [SerializeField]
    private GameObject _inviteFriendsPanel = default;
    [SerializeField]
    private GameObject _leaderboardPanel = default;
    [SerializeField]
    private GameObject _mapPanel = default;
    [SerializeField]
    private GameObject _profilePanel = default;
    [SerializeField]
    private GameObject _shopPanel = default;
    [SerializeField]
    private GameObject _villagePanel = default;
    [SerializeField]
    private GameObject _settingsPanel = default;

    private Animator _animator = default;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }


    public void ActivatePanelFromButtons(string panelName)
    {
        foreach (EnumData.MenuPanelsName item in System.Enum.GetValues(typeof(EnumData.MenuPanelsName)))
        {
            if (item.ToString() == panelName)
            {
                ActivateUIPanel(item);
            }
        }
    }

    public void ActivateUIPanel(EnumData.MenuPanelsName panelName)
    {
        switch (panelName)
        {
            case EnumData.MenuPanelsName.SpinPanel: CloseAllPanels(); _spinPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.GiftsPanel: CloseAllPanels(); _giftsPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.CardCollectionPanel: CloseAllPanels(); _cardCollectionPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.DailySpinPanel: CloseAllPanels(); _dailySpinPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.InviteFriendsPanel: CloseAllPanels(); _inviteFriendsPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.LeaderboardPanel: CloseAllPanels(); _leaderboardPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.MapPanel: CloseAllPanels(); _mapPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.ProfilePanel: CloseAllPanels(); _profilePanel.SetActive(true); break;
            case EnumData.MenuPanelsName.ShopPanel: CloseAllPanels(); _shopPanel.SetActive(true); break;
            case EnumData.MenuPanelsName.VillagePanel: CloseAllPanels(); _villagePanel.SetActive(true); break;
            case EnumData.MenuPanelsName.SettingsPanel: CloseAllPanels(); _settingsPanel.SetActive(true); break;
        }
        _animator.SetTrigger("CloseMenu");
    }

    public void CloseAllPanels()
    {
        _spinPanel.SetActive(false);
        _giftsPanel.SetActive(false);
        _cardCollectionPanel.SetActive(false);
        _dailySpinPanel.SetActive(false);
        _inviteFriendsPanel.SetActive(false);
        _leaderboardPanel.SetActive(false);
        _mapPanel.SetActive(false);
        _profilePanel.SetActive(false);
        _shopPanel.SetActive(false);
        _villagePanel.SetActive(false);
        _settingsPanel.SetActive(false);
    }

}
