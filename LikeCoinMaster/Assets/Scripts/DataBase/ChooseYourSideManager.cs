﻿using Firebase.Database;
using UnityEngine;
using UnityEngine.UI;

public class ChooseYourSideManager : MonoBehaviour
{
    [SerializeField]
    private Button _lightSideButton = default;
    [SerializeField]
    private Button _darkSideButton = default;
    [SerializeField]
    private GameObject _chooseYourSidePanel = default;

    private void Start()
    {
        if (PlayerPrefs.GetInt("FirstTime") == 0)
        {
            _chooseYourSidePanel.SetActive(true);
            
        }

        _lightSideButton.onClick.AddListener(() => LightSideComplete());
        _darkSideButton.onClick.AddListener(() => DarkSideComplete());
    }
    private void LightSideComplete()
    {
        FindObjectOfType<EventLiveStatusUI>().UpdateLightWizardsNumber(1);
        PlayerPrefs.SetInt("FirstTime", 1);
    }
    private void DarkSideComplete()
    {
        FindObjectOfType<EventLiveStatusUI>().UpdateDarkWizardsNumber(1);
        PlayerPrefs.SetInt("FirstTime", 1);
    }
    public void UpdatePlayer(EventData player)
    {
        FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string jsonPlayer = JsonUtility.ToJson(player);

        reference.Child("EventsData").Child(player.CounterEvent.ToString()).SetRawJsonValueAsync(jsonPlayer);
    }

}
