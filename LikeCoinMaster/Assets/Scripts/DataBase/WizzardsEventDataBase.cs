﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizzardsEventDataBase : MonoBehaviour
{
    [SerializeField]
    private EventData[] _eventsData = default;

    [SerializeField]
    private EventData _currentEventData = default;



    public void CloseAnEvent(int eventNumber)
    {
        for (int i = 0; i < _eventsData.Length; i++)
        {
            //if (_eventsData[i].CounterEvent == eventNumber)
            //{
            //    _eventsData[i].EventIsClosed = true;
            //}
        }
    }
    public EventData CurrentEventData()
    {
        for (int i = 0; i < _eventsData.Length; i++)
        {
            //if (_eventsData[i].EventIsClosed == false)
            //{
            //    return _eventsData[i];
            //}
        }
        return null;
    }
    public void CurrentEventIsLoaded(EventData eventData)
    {
        _currentEventData = eventData;
    }
    public void LightOrbUpdate()
    {
        FindObjectOfType<EventLiveStatusUI>().UpdateLightPower();
    }
    public void DarkOrbUpdate()
    {
        FindObjectOfType<EventLiveStatusUI>().UpdateDarkPower();
    }

}
[System.Serializable]
public class EventData
{
    public string EventIsClosed = default;
    public string CounterEvent = default;
    public string MaxUsersInTheRoom = "15";
    public string LightWizards = default;
    public string LightOrbsCollected = default;
    public string DarkWizards = default;
    public string DarkOrbsCollected = default;
    
    public EventData(string eventIsClosed, string counterEvent, string maxUserInTheRoom, string lightWizards, string lightOrbs, string darkWizards, string darkOrbs)
    {
        this.EventIsClosed = eventIsClosed;
        this.CounterEvent = counterEvent;
        this.MaxUsersInTheRoom = maxUserInTheRoom;
        this.LightWizards = lightWizards;
        this.LightOrbsCollected = lightOrbs;
        this.DarkWizards = darkWizards;
        this.DarkOrbsCollected = darkOrbs;
    }
}

[System.Serializable]
public class RewardData
{
    [SerializeField]
    private int _numberEvent = default;
    [SerializeField]
    private int _numberOrbs = default;
    [SerializeField]
    private int _numberMoney = default;
    [SerializeField]
    private int _numberXp = default;
    [SerializeField]
    private int _numberSpins = default;

    public int NumberEvent
    {
        get
        {
            return _numberEvent;
        }
    }
    public int NumberOrbs
    {
        get
        {
            return _numberOrbs;
        }
    }
    public int NumberMoney
    {
        get
        {
            return _numberMoney;
        }
    }
    public int NumberXP
    {
        get
        {
            return _numberXp;
        }
    }
    public int NumberSpins
    {
        get
        {
            return _numberSpins;
        }
    }
}
