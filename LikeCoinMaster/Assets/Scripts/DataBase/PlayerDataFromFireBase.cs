﻿[System.Serializable]
public class PlayerDataFromFireBase
{
    public string ID = default;
    public string NAME = default;
    public string COINS = default;
    public string STARS = default;
    public string SHIELDS = default;
    public string SIDE = default;

    public PlayerDataFromFireBase(string id, string name, string coins, string stars, string shields,string side)
    {
        this.ID = id;
        this.NAME = name;
        this.COINS = coins;
        this.STARS = stars;
        this.SHIELDS = shields;
        this.SIDE = side;
    }
}
