﻿using System.IO;
using UnityEngine;

public class PlayerDataUI : MonoBehaviour
{
    [SerializeField]
    private PlayerData _currentPlayerData = default;
    [SerializeField]
    private PlayerDataOffline _currentPlayerOfflineData = default;
    private string jsonData = "";
    private string _path = "";
    public PlayerData Data
    {
        get
        {
            return _currentPlayerData;
        }
        set
        {
            _currentPlayerData = value;
        }
    }
    public PlayerDataOffline OfflineData
    {
        get
        {
            return _currentPlayerOfflineData;
        }
        set
        {
            _currentPlayerOfflineData = value;           
        }
    }
    private void Awake()
    {
        _path = Application.persistentDataPath + "/PlayerOfflineData.json";
        GameObject[] objs = GameObject.FindGameObjectsWithTag("PlayerData");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
    private void Start()
    {
        LoadGame();
    }
    public void SaveGame()
    {
        jsonData = JsonUtility.ToJson(_currentPlayerOfflineData);
        File.WriteAllText(_path, jsonData);
        Debug.Log("SaveGame");
    }
    private void LoadGame()
    {
        if (File.Exists(_path))
        {
            var loadData = File.ReadAllText(_path);
            PlayerDataOffline playerData = JsonUtility.FromJson<PlayerDataOffline>(loadData);
            _currentPlayerOfflineData = playerData;
        }
    }
    public void ExtractDataAndLoadInCurrentUser(PlayerDataFromFireBase playerFromFireBase)
    {
        _currentPlayerData.ID = playerFromFireBase.ID;
        _currentPlayerData.NAME = playerFromFireBase.NAME;
        _currentPlayerData.COINS = playerFromFireBase.COINS;
        _currentPlayerData.STARS = playerFromFireBase.STARS;
        _currentPlayerData.SHIELDS = playerFromFireBase.SHIELDS;
    }
}
