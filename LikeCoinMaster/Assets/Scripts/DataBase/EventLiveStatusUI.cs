﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventLiveStatusUI : MonoBehaviour
{
    [SerializeField]
    private Slider _eventSlider = default;
    [SerializeField]
    private WizzardsEventDataBase _eventsDataBase = default;
    [SerializeField]
    List<EventData> Events = new List<EventData>();
    [SerializeField]
    private int _eventSelect = default;

    [SerializeField]
    private EventData _currentEventData = default;
    [SerializeField]
    private PlayerDataFromFireBase _currentDataPlayer = default;

    private bool _loadComplete = default;


    private float _coolDown = 1f;
    private float _timer = default;
    [SerializeField]
    private bool _firstTimeEnter = default;


    public string CurrentUserID = default;


    private bool ConvertStringToBool(string data)
    {
        if (bool.Parse(data) == false)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    private void Start()
    {
        ExtractDataFromFireBase();
    }
    private void ExtractDataFromFireBase()
    {
        FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
        dbInstance.GetReference("EventsData").GetValueAsync().ContinueWith(
            task =>
            {
                if (task.IsFaulted)
                {
                    Debug.Log(task.Result);
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    Debug.Log(snapshot.GetRawJsonValue());
                    foreach (var item in snapshot.Children)
                    {
                        Debug.Log(item.GetRawJsonValue());
                        EventData eventdata = JsonUtility.FromJson<EventData>(item.GetRawJsonValue());
                        Events.Add(eventdata);
                    }
                }

                UpdateCurrentEvent();
                _timer = _coolDown;
            }
            );
    }
    private void UpdateCurrentEventStatus()
    {
        if (_loadComplete)
        {
            FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
            dbInstance.GetReference("EventsData").GetValueAsync().ContinueWith(
                task =>
                {
                    if (task.IsFaulted)
                    {
                        Debug.Log(task.Result);
                    }
                    else if (task.IsCompleted)
                    {
                        DataSnapshot snapshot = task.Result;
                        foreach (var item in snapshot.Children)
                        {
                            Debug.Log(item.GetRawJsonValue());
                            EventData eventdata = JsonUtility.FromJson<EventData>(item.GetRawJsonValue());
                            Events[_eventSelect] = eventdata;
                            _currentEventData = eventdata;
                        }
                    }
                }
                );
        }
    }
    public void CreateANewPlayer(PlayerData player)
    {
        FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string jsonPlayer = JsonUtility.ToJson(player);
        reference.Child("Players").Child(player.ID.ToString()).SetRawJsonValueAsync(jsonPlayer);
    }
    private void UpdateCurrentEvent()
    {
        _currentEventData = Events[_eventSelect];
        _loadComplete = true;
    }
    void Update()
    {
        if (_loadComplete)
        {
            int lightOrbs = Convert.ToInt32(Events[_eventSelect].LightOrbsCollected);
            int darkOrbs = Convert.ToInt32(Events[_eventSelect].DarkOrbsCollected);

            CalculateTheSliderValue(lightOrbs, darkOrbs);
        }
        if (_timer <= 0)
        {
            UpdateCurrentEventStatus();
            _timer = _coolDown;
        }
        else
        {
            _timer -= Time.deltaTime;
        }
    }
    private void CalculateTheSliderValue(int lightOrbs, int darkOrbs)
    {
        if (lightOrbs == 0 && darkOrbs == 0)
        {
            _eventSlider.value = 50;
            return;
        }

        int max = lightOrbs + darkOrbs;
        float lightPercents = lightOrbs * 100 / max;
        _eventSlider.value = lightPercents;
    }
    public void UpdateLightPower()
    {
        int lightOrbs = Convert.ToInt32(_currentEventData.LightOrbsCollected);
        lightOrbs++;
        Events[_eventSelect].LightOrbsCollected = lightOrbs.ToString();
        UpdatePlayer(Events[_eventSelect]);
    }
    public void UpdateDarkPower()
    {
        int darkOrbs = Convert.ToInt32(_currentEventData.DarkOrbsCollected);
        darkOrbs++;
        Events[_eventSelect].DarkOrbsCollected = darkOrbs.ToString();
        UpdatePlayer(Events[_eventSelect]);
    }
    public void UpdatePlayer(EventData player)
    {
        FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string jsonPlayer = JsonUtility.ToJson(player);

        reference.Child("EventsData").Child(player.CounterEvent.ToString()).SetRawJsonValueAsync(jsonPlayer);
    }
    public EventData GetCurrentData()
    {
        return _currentEventData;
    }
    public void UpdateLightWizardsNumber(int wizards)
    {
        int wizardsLight = Convert.ToInt32(_currentEventData.LightWizards);
        wizardsLight+= wizards;
        Events[_eventSelect].LightWizards = wizardsLight.ToString();
        UpdatePlayer(Events[_eventSelect]);
    }
    public void UpdateDarkWizardsNumber(int wizards)
    {
        int wizardsDark = Convert.ToInt32(_currentEventData.DarkWizards);
        wizardsDark += wizards;
        Events[_eventSelect].DarkWizards = wizardsDark.ToString();
        UpdatePlayer(Events[_eventSelect]);
    }
    public void GetCurrentPlayerData()
    {
        bool playerIsRegister = false;
        var PlayerDataBase = FindObjectOfType<DataBaseUI>().FireBaseLoadPlayersData;
        //Try to find if the player is in the database
        foreach (var item in PlayerDataBase)
        {
            if (item.ID == CurrentUserID)
            {
                _currentDataPlayer = item;
                playerIsRegister = true;
            }
        }
        //If you are here that the player is not in the data base
        //So we create a new user
        if (!playerIsRegister)
        {
            _currentDataPlayer.ID = UnityEngine.Random.Range(0, 10).ToString() + UnityEngine.Random.Range(0, 10).ToString() + UnityEngine.Random.Range(0, 10).ToString() + UnityEngine.Random.Range(0, 10).ToString();
            _currentDataPlayer.COINS = "0";
            _currentDataPlayer.NAME = "NoName";
            _currentDataPlayer.SHIELDS = "0";
            _currentDataPlayer.STARS = "0";
            _currentDataPlayer.SIDE = "None";
        }
    }
}
