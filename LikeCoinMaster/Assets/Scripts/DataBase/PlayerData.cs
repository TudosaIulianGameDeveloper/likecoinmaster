﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName ="ScriptableObjects/PlayerData",order = 1)]
[System.Serializable]
public class PlayerData : ScriptableObject
{
    public string ID = default;
    public string NAME = default;
    public string COINS = default;
    public string STARS = default;
    public string SHIELDS = default;
    public string SIDE = default;

    public PlayerData(string id, string name, string coins, string stars, string shields,string side)
    {
        this.ID = id;
        this.NAME = name;
        this.COINS = coins;
        this.STARS = stars;
        this.SHIELDS = shields;
        this.SIDE = side;
    }
}
