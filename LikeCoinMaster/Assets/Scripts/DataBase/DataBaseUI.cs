﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using System;
using System.Xml;

public class DataBaseUI : MonoBehaviour
{
    [SerializeField]
    private DataBase _dataBase = default;
    [SerializeField]
    private List<PlayerDataFromFireBase> _fireBaseLoadData = default;
    [SerializeField]
    private PlayerDataUI _playerUIData = default;

    public List<PlayerDataFromFireBase> FireBaseLoadPlayersData
    {
        get
        {
            return _fireBaseLoadData;
        }
        set
        {
            _fireBaseLoadData = value;
        }
    }
    private void Start()
    {
        //LoadPlayers();
    }
    public bool CheckIfTheUserIsInTheDataBase(string ID)
    {
        foreach (var item in _fireBaseLoadData)
        {
            if (item.ID == ID)
            {
                //_playerUIData.ExtractDataAndLoadInCurrentUser(item);
                Debug.Log("Exist");
                return true;
            }
        }
        return false;
    }
    public PlayerData ExtractData(string ID)
    {
        foreach (var item in _fireBaseLoadData)
        {
            if (item.ID == ID)
            {
                _playerUIData.ExtractDataAndLoadInCurrentUser(item);
                FindObjectOfType<LoginSceneManager>().CurrentPlayerData.Data = FindObjectOfType<PlayerDataUI>().Data;
            }
        }
        return null;
    }
    public void LoadPlayers()
    {
        FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
        dbInstance.GetReference("Players").OrderByChild("ID").GetValueAsync().ContinueWith(
            task =>
            {
                if (task.IsFaulted)
                {
                    Debug.Log(task.Result);
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    Debug.Log(snapshot.GetRawJsonValue());
                    foreach (var item in snapshot.Children)
                    {
                        Debug.Log(item.GetRawJsonValue());
                        PlayerDataFromFireBase player = JsonUtility.FromJson<PlayerDataFromFireBase>(item.GetRawJsonValue());
                        player.ID = item.Key;
                        _fireBaseLoadData.Add(player);
                    }
                    foreach (var item in _fireBaseLoadData)
                    {
                        Debug.Log(item.ID + " " + item.NAME);
                    }
                }
            }
            );
    }

    public void UpdatePlayer(PlayerData player)
    {
        FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string jsonPlayer = JsonUtility.ToJson(player);

        reference.Child("Players").Child(player.ID.ToString()).SetRawJsonValueAsync(jsonPlayer);
    }
    public void AddPlayer(PlayerData player)
    {
        FirebaseDatabase dbInstance = FirebaseDatabase.DefaultInstance;
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        string jsonPlayer = JsonUtility.ToJson(player);
        reference.Child("Players").Child(player.ID.ToString()).SetRawJsonValueAsync(jsonPlayer);
    }
    public void AddMe()
    {
        AddPlayer(new PlayerData("2", "Iulian", "200", "32", "3", "Dark"));
    }
}
