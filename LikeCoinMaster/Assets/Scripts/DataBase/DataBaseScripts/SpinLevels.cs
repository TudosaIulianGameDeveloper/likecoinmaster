﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinLevels : MonoBehaviour
{
    [SerializeField]
    private Slider _sliderLevelSpin = default;
    [SerializeField]
    private SpinLevel[] _spinData = default;
    [SerializeField]
    private int _currentLevel = default;

    private void Awake()
    {
        StartNewLevel();
    }
    public void StartNewLevel()
    {
        _sliderLevelSpin.maxValue = _spinData[_currentLevel].NumberNeeded;
        _sliderLevelSpin.value = 0;
    }
    public void UpdateSlider(int newValue,string item)
    {
        if (item == _spinData[_currentLevel].ItemName)
        {
            _sliderLevelSpin.value += newValue;
            if (_sliderLevelSpin.value >= _sliderLevelSpin.maxValue)
            {
                _currentLevel++;
                StartNewLevel();
            }
        }
    }

}

[System.Serializable]
public class SpinLevel
{
    public string ItemName = default;
    public int NumberNeeded = default;
}
