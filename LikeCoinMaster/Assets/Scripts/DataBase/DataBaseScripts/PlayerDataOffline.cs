﻿
[System.Serializable]
public class PlayerDataOffline
{

    public string ID = default;
    public string NAME = default;
    public string COINS = default;
    public string ORBS = default;
    public string ORBSCOLLECTED = default;
    public string SPINS = default;
    public string DIAMONDS = default;
    public string MONEY = default;
    public string POTIONS = default;
    public string SHIELDS = default;
    public string EXPERIENCE = default;
    public string LEVEL = default;

    public void UpdateCoinsUI(int amount)
    {
        int oldCoins;
        if (COINS == string.Empty)
        {
            oldCoins = 0;
        }
        else
        {
            oldCoins = System.Convert.ToInt32(COINS);
        }
        oldCoins += amount;
        COINS = oldCoins.ToString();
    }
    public void UpdateDiamondsUI(int amount)
    {
        int oldDiamonds;
        if (DIAMONDS == string.Empty)
        {
            oldDiamonds = 0;
        }
        else
        {
            oldDiamonds = System.Convert.ToInt32(DIAMONDS);
        }
        oldDiamonds += amount;
        DIAMONDS = oldDiamonds.ToString();
    }
    public void UpdateExperienceUI(int amount)
    {
        int oldExp;
        if (EXPERIENCE == string.Empty)
        {
            oldExp = 0;
        }
        else
        {
            oldExp = System.Convert.ToInt32(EXPERIENCE);
        }
        oldExp += amount;
        EXPERIENCE = oldExp.ToString();
    }
}
