﻿using UnityEngine;
using Firebase.Auth;
using UnityEngine.UI;
using TMPro;
using System;

public class AuthController : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField _emailInput = default;
    [SerializeField]
    private TMP_InputField _passwordInput = default;
    [SerializeField]
    private Button _loginButton = default;
    [SerializeField]
    private Button _registerButton = default;
    [SerializeField]
    private Button _backAndLogout = default;
    [SerializeField]
    private Button _loginAnon = default;
    [Header("CurrentPlayer")]
    [SerializeField]
    private LoginSceneManager _loginManager = default;

    private DataBaseUI _dataBaseUI = default;
    [SerializeField]
    private bool _loginSucces = default;

    private bool _logoutActivate = default;
    public bool LoginSucces
    {
        get
        {
            return _loginSucces;
        }
        set
        {
            _loginSucces = value;
        }
    }
    private void Awake()
    {
        if (_dataBaseUI == null)
        {
            _dataBaseUI = FindObjectOfType<DataBaseUI>();
        }
    }
    private void Update()
    {
        if (LoginSucces)
        {
            _loginButton.gameObject.SetActive(false);
            _loginAnon.gameObject.SetActive(false);
            _registerButton.gameObject.SetActive(false);
            LoginSucces = false;
        }
        if (_logoutActivate)
        {
            _logoutActivate = false;
            _loginManager.ActivateMainLoginPanels(MainPanelsNames.LoginSection);
        }
    }
    private void Start()
    {
        ButtonsAction();
        Debug.Log("<color=red>CurrentUser: " + _loginManager.CurrentPlayerData.Data.ID+"</color>");
        Debug.Log("Current User from data   " + FindObjectOfType<PlayerDataUI>().Data.ID);
    }
    public void ActivateFireBaseButtonsUI()
    {
        _loginButton.gameObject.SetActive(true);
        _loginAnon.gameObject.SetActive(true);
        _registerButton.gameObject.SetActive(true);
    }
    private void ButtonsAction()
    {
        _loginButton.onClick.AddListener(() => Login());
        _loginAnon.onClick.AddListener(() => Login_Anon());
        _registerButton.onClick.AddListener(() => RegisterUser());
        _backAndLogout.onClick.AddListener(() => Logout());
    }

    public void Login()
    {
        FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(_emailInput.text, _passwordInput.text).ContinueWith(
            task =>
            {
                if (task.IsCanceled)
                {
                    Firebase.FirebaseException e = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                    GetErrorMessage((AuthError)e.ErrorCode);
                    Debug.Log("Cancel");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.Log("<color=red>Error</color>");
                    Firebase.FirebaseException e = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    GetErrorMessage((AuthError)e.ErrorCode);
                    return;
                }
                if (task.IsCompleted)
                {
                    print("User is Loged In");
                    print(task.Result.UserId);
                    if (_dataBaseUI.CheckIfTheUserIsInTheDataBase(task.Result.UserId))
                    {
                        LoginSucces = true;
                        _dataBaseUI.ExtractData(task.Result.UserId);
                        _loginManager.IsANewUser = false;
                    }
                }
                _loginManager.IsANewUser = false;
            });
    }

    public void Login_Anon()
    {
        Debug.Log("Current user anon logged " + " " + PlayerPrefs.GetInt("AnonymID").ToString());
        if (PlayerPrefs.GetInt("AnonymID") == 0)
        {
            FirebaseAuth.DefaultInstance.SignInAnonymouslyAsync().ContinueWith((task =>
            {
                if (task.IsCanceled)
                {
                    Firebase.FirebaseException e = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                    GetErrorMessage((AuthError)e.ErrorCode);
                    Debug.Log("Cancel");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.Log("<color=red>Error</color>");
                    Firebase.FirebaseException e = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    GetErrorMessage((AuthError)e.ErrorCode);
                    return;
                }
                if (task.IsCompleted)
                {
                    print("User is Loged In Anonymus");
                    print("User is Loged In");
                    print(task.Result.UserId);
                    /*
                    bool isInTheDataBase = false;
                    if (_dataBaseUI.CheckIfTheUserIsInTheDataBase(_loginManager.CurrentPlayerData.Data.ID))
                    {
                        LoginSucces = true;
                        _dataBaseUI.ExtractData(_loginManager.CurrentPlayerData.Data.ID);
                        _loginManager.IsANewUser = false;
                        isInTheDataBase = true;
                    }
                    if (!isInTheDataBase)
                    {
                        if (_dataBaseUI.CheckIfTheUserIsInTheDataBase(task.Result.UserId))
                        {
                            LoginSucces = true;
                            _dataBaseUI.ExtractData(task.Result.UserId);
                            _loginManager.IsANewUser = false;
                        }
                        else
                        {
                            RegisterAnAnonymus(task.Result.UserId, "87654321");
                        }
                    }
                    PlayerPrefs.SetInt("AnonymID", 1);
                    */
                    LoginSucces = true;
                }
            }));
        }
        else
        {
            if (_dataBaseUI.CheckIfTheUserIsInTheDataBase(_loginManager.CurrentPlayerData.Data.ID))
            {
                _dataBaseUI.ExtractData(_loginManager.CurrentPlayerData.Data.ID);
                _loginManager.IsANewUser = false;
            }
            else
            {
                _loginManager.CurrentPlayerData.Data.ID = _loginManager.CurrentPlayerData.Data.ID;
                _loginManager.IsANewUser = true;
            }
        }
    }

    public void Logout()
    {
        if (FirebaseAuth.DefaultInstance.CurrentUser != null)
        {
            FirebaseAuth.DefaultInstance.SignOut();
        }

        _logoutActivate = true;
    }
    public void RegisterUser()
    {
        if (_emailInput.text.Equals("") && _passwordInput.text.Equals(""))
        {
            print("Please enter a valid email and pass to register");
            return;
        }
        FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(_emailInput.text, _passwordInput.text).ContinueWith((task =>
         {
             if (task.IsCanceled)
             {
                 Debug.Log("Cancel");
                 return;
             }
             if (task.IsFaulted)
             {
                 Debug.Log("<color=red>Error</color>");
                 Firebase.FirebaseException e = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                 GetErrorMessage((AuthError)e.ErrorCode);
                 return;
             }
             if (task.IsCompleted)
             {
                 print("Registration complete!!!!");

                 print(task.Result.Email.ToString());
                 _loginManager.CurrentPlayerData.Data.ID = task.Result.UserId.ToString();

                 String[] parts = task.Result.Email.ToString().Split(new[] { '@' });
                 String username = parts[0];
                 String domain = parts[1];
                 _loginManager.CurrentPlayerData.Data.NAME = username;
                 _loginManager.CurrentPlayerData.Data.COINS = "0";
                 _loginManager.CurrentPlayerData.Data.STARS = "0";
                 _loginManager.CurrentPlayerData.Data.SHIELDS = "0";
                 _loginManager.CurrentPlayerData.Data.SIDE = "Light";
                 _loginManager.IsANewUser = true;
             }
         }));
    }
    private void RegisterAnAnonymus(string id,string pass)
    {
        id += "@WizzCoin.ro";
        FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(id, pass).ContinueWith((task =>
        {
            if (task.IsCanceled)
            {
                Debug.Log("Cancel");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.Log("<color=red>Error</color>");
                Firebase.FirebaseException e = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                GetErrorMessage((AuthError)e.ErrorCode);
                return;
            }
            if (task.IsCompleted)
            {
                print("Registration complete!!!!");

                print(task.Result.Email.ToString());
                _loginManager.CurrentPlayerData.Data.ID = task.Result.UserId.ToString();

                String[] parts = task.Result.Email.ToString().Split(new[] { '@' });
                String username = parts[0];
                String domain = parts[1];
                _loginManager.CurrentPlayerData.Data.NAME = username;
                _loginManager.CurrentPlayerData.Data.COINS = "0";
                _loginManager.CurrentPlayerData.Data.STARS = "0";
                _loginManager.CurrentPlayerData.Data.SHIELDS = "0";
                _loginManager.CurrentPlayerData.Data.SIDE = "Light";
                _loginManager.IsANewUser = true;
            }
        }));
    }

    private void GetErrorMessage(AuthError errorCode)
    {
        string msg = "";
        msg = errorCode.ToString();
        switch (errorCode)
        {
            case AuthError.AccountExistsWithDifferentCredentials:
                print("Account is aleardy exist but with different credentials");
                break;
            case AuthError.MissingPassword:
                print(msg);
                break;
            case AuthError.WrongPassword:
                print(msg);
                break;
            case AuthError.InvalidEmail:
                print(msg);
                break;

        }
        print(msg);
    }
}
