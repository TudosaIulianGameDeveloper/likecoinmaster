﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdminScripts : MonoBehaviour
{
    [SerializeField]
    private int _openMenuPanel = default;
    [SerializeField]
    private Animator _menuAnimation = default;


    void Update()
    {
        OpenMenuPanelWithAnimation();
    }

    private void OpenMenuPanelWithAnimation()
    {
        if (_openMenuPanel==1)
        {
            _menuAnimation.SetTrigger("OpenMenu");
            _openMenuPanel = 0;
        }
        else if (_openMenuPanel == 2)
        {
            _menuAnimation.SetTrigger("CloseMenu");
            _openMenuPanel = 0;
        }
    }
}
