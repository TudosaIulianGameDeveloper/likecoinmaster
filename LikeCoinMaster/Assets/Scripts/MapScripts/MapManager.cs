﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class MapManager : MonoBehaviour
{
   
    [SerializeField]
    private PlayerDataUI PlayerDataOffline = default;
    public MapManagerData MapManagerStatus = default;



    [Range(0,3)]
    public int TownSelected = default;
  
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            UpdateTower(TownSelected);
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            Debug.Log("<color=red> Production " + MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[TownSelected].Prouction+"</color>");
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            CompleteAZone();
            Debug.Log("CompleteZone? " + " currentZone " + MapManagerStatus.CurrentZoneSelected);
        }
    }
    public void CompleteAZone()
    {
        int isZoneIsComplete = 0;
        for (int i = 0; i < MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData.Length; i++)
        {
            if (MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[i].TowerComplete)
            {
                isZoneIsComplete++;
            }
        }
        if (isZoneIsComplete == 4)
        {
            //NextZone
            MapManagerStatus.CurrentZoneSelected++;
        }
    }
    public void UpdateTower(int index)
    {
        if (MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].IsMainTower && MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].CurrentLevel == 2)
        {
            Debug.Log("This town is on the max level");
            return;
        }
        else if (!MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].IsMainTower && MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].CurrentLevel == 1)
        {
            Debug.Log("This town is on the max level");
            return;
        }
        MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].CurrentLevel++;
        MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersObjects[index].GetComponent<SpriteRenderer>().sprite= MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].Needs[MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].CurrentLevel].Images;
        GetExperienceForUserAfterCompleteATownLevel(MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].Needs[MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[index].CurrentLevel].Experience);
    }

    public float GetMainTowerProduction()
    {
        for (int i = 0; i < MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData.Length; i++)
        {
            if (MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[i].IsMainTower)
            {
                return MapManagerStatus.ZonesData[MapManagerStatus.CurrentZoneSelected].TowersData[i].Prouction;
            }
        }
        return 0;
    }
    private void GetExperienceForUserAfterCompleteATownLevel(float Exp)
    {
        //Need PlayerData
        PlayerDataOffline.OfflineData.UpdateExperienceUI((int)Exp);
        PlayerDataOffline.SaveGame();
    }
}
[System.Serializable]
public class MapManagerData
{
    public int CurrentZoneSelected = 0;
    public ZonesManager[] ZonesData = default;
}

[System.Serializable]
public class ZonesManager
{
    public TowersManager[] TowersData = default;
    public GameObject[] TowersObjects = default;
}

[System.Serializable]
public class TowersManager
{
    public TowerNeeds[] Needs = default;
    public int CurrentLevel = 0;
    public bool IsMainTower = default;
    public bool TowerComplete = default;
    public float Prouction = default;
}
[System.Serializable]
public class TowerNeeds
{
    public int Money = default;
    public Sprite Images = default;
    public float Experience = default;
    public int FirePotion = default;
    public int WaterPotion = default;
    public int EarthPotion = default;
    public int AirPotion = default;
}
