﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinManager : MonoBehaviour
{

    public static event Action HandlePulled = delegate { };

    [SerializeField]
    private Text _prizeText = default;
    [SerializeField]
    private RowController[] _rows = default;
    [SerializeField]
    private Transform _handle = default;

    private int _prizeValue = default;
    private bool _resultsChecked = false;

    [SerializeField]
    private PlayerDataUI _playerData = default;

    private void Update()
    {
        if (!_rows[0].RowStopped || !_rows[1].RowStopped || !_rows[2].RowStopped)
        {
            _prizeValue = 0;
            _prizeText.enabled = false;
            _resultsChecked = false;
        }
        if (_rows[0].RowStopped && _rows[1].RowStopped && _rows[2].RowStopped && !_resultsChecked)
        {
            CheckResults();
            _prizeText.enabled = true;
            _prizeText.text = "Prize: " + _prizeValue;
        }
    }
    private void OnMouseDown()
    {
        if (_rows[0].RowStopped && _rows[1].RowStopped && _rows[2].RowStopped)
        {
            StartCoroutine("PullHandle");
        }
    }
    private IEnumerator PullHandle()
    {
        for (int i = 0; i < 15; i += 5)
        {
            _handle.Rotate(0f, 0f, i);
            yield return new WaitForSeconds(0.1f);
        }
        HandlePulled();
        for (int i = 0; i < 15; i+=5)
        {
            _handle.Rotate(0f, 0f, -i);
            yield return new WaitForSeconds(0.1f);
        }
    }
    private void CheckResults()
    {
        if (_rows[0].StoppedSlot == "Diamond" && _rows[1].StoppedSlot == "Diamond" && _rows[2].StoppedSlot == "Diamond")
        {
            _prizeValue = 200;
        }
        else if (_rows[0].StoppedSlot == "Crown" && _rows[1].StoppedSlot == "Crown" && _rows[2].StoppedSlot == "Crown")
        {
            _prizeValue = 400;
        }
        else if (_rows[0].StoppedSlot == "Melon" && _rows[1].StoppedSlot == "Melon" && _rows[2].StoppedSlot == "Melon")
        {
            _prizeValue = 600;
        }
        else if (_rows[0].StoppedSlot == "Bar" && _rows[1].StoppedSlot == "Bar" && _rows[2].StoppedSlot == "Bar")
        {
            _prizeValue = 800;
        }
        else if (_rows[0].StoppedSlot == "Seven" && _rows[1].StoppedSlot == "Seven" && _rows[2].StoppedSlot == "Seven")
        {
            _prizeValue = 1500;
        }
        else if (_rows[0].StoppedSlot == "Cherry" && _rows[1].StoppedSlot == "Cherry" && _rows[2].StoppedSlot == "Cherry")
        {
            _prizeValue = 3000;
        }
        else if (_rows[0].StoppedSlot == "Lemon" && _rows[1].StoppedSlot == "Lemon" && _rows[2].StoppedSlot == "Lemon")
        {
            _prizeValue = 5000;
        }
        else if (((_rows[0].StoppedSlot == _rows[1].StoppedSlot)
            && (_rows[0].StoppedSlot == "Diamond"))
           || ((_rows[0].StoppedSlot == _rows[2].StoppedSlot)
            && (_rows[0].StoppedSlot == "Diamond"))
            || ((_rows[1].StoppedSlot == _rows[2].StoppedSlot)
            && (_rows[1].StoppedSlot == "Diamond")))
        {
            _prizeValue = 100;
        }
        else if (((_rows[0].StoppedSlot == _rows[1].StoppedSlot)
           && (_rows[0].StoppedSlot == "Crown"))
          || ((_rows[0].StoppedSlot == _rows[2].StoppedSlot)
           && (_rows[0].StoppedSlot == "Crown"))
           || ((_rows[1].StoppedSlot == _rows[2].StoppedSlot)
           && (_rows[1].StoppedSlot == "Crown")))
        {
            _prizeValue = 300;
        }
        else if (((_rows[0].StoppedSlot == _rows[1].StoppedSlot)
          && (_rows[0].StoppedSlot == "Melon"))
         || ((_rows[0].StoppedSlot == _rows[2].StoppedSlot)
          && (_rows[0].StoppedSlot == "Melon"))
          || ((_rows[1].StoppedSlot == _rows[2].StoppedSlot)
          && (_rows[1].StoppedSlot == "Melon")))
        {
            _prizeValue = 500;
        }
        else if (((_rows[0].StoppedSlot == _rows[1].StoppedSlot)
        && (_rows[0].StoppedSlot == "Bar"))
       || ((_rows[0].StoppedSlot == _rows[2].StoppedSlot)
        && (_rows[0].StoppedSlot == "Bar"))
        || ((_rows[1].StoppedSlot == _rows[2].StoppedSlot)
        && (_rows[1].StoppedSlot == "Bar")))
        {
            _prizeValue = 700;
        }
        else if (((_rows[0].StoppedSlot == _rows[1].StoppedSlot)
        && (_rows[0].StoppedSlot == "Seven"))
       || ((_rows[0].StoppedSlot == _rows[2].StoppedSlot)
        && (_rows[0].StoppedSlot == "Seven"))
        || ((_rows[1].StoppedSlot == _rows[2].StoppedSlot)
        && (_rows[1].StoppedSlot == "Seven")))
        {
            _prizeValue = 1000;
        }
        else if (((_rows[0].StoppedSlot == _rows[1].StoppedSlot)
        && (_rows[0].StoppedSlot == "Cherry"))
       || ((_rows[0].StoppedSlot == _rows[2].StoppedSlot)
        && (_rows[0].StoppedSlot == "Cherry"))
        || ((_rows[1].StoppedSlot == _rows[2].StoppedSlot)
        && (_rows[1].StoppedSlot == "Cherry")))
        {
            _prizeValue = 2000;
        }
        else if (((_rows[0].StoppedSlot == _rows[1].StoppedSlot)
     && (_rows[0].StoppedSlot == "Lemon"))
    || ((_rows[0].StoppedSlot == _rows[2].StoppedSlot)
     && (_rows[0].StoppedSlot == "Lemon"))
     || ((_rows[1].StoppedSlot == _rows[2].StoppedSlot)
     && (_rows[1].StoppedSlot == "Lemon")))
        {
            _prizeValue = 4000;
        }
        _resultsChecked = true;
        _playerData.OfflineData.UpdateCoinsUI(_prizeValue);
        _playerData.SaveGame();
    }
}
