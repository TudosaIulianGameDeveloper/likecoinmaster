﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowController : MonoBehaviour
{

    [SerializeField]
    private bool _rowStopped = default;
    [SerializeField]
    private string _stoppedSlot = default;
    [SerializeField]
    private float _minimumRange = default;
    [SerializeField]
    private float _maximumRange = default;

    [SerializeField]
    private int _diamondPercent = default;
    [SerializeField]
    private int _crownPercent = default;
    [SerializeField]
    private int _melonPercent = default;
    [SerializeField]
    private int _barPercent = default;
    [SerializeField]
    private int _sevenPercent = default;
    [SerializeField]
    private int _cherryPercent = default;
    [SerializeField]
    private int _lemonPercent = default;

    [SerializeField]
    private int _methodSelected = default;

    [SerializeField]
    private List<RandomizeData> _randomizeData = default;


    private int _randomValue = default;
    private float _timeInterval = default;
    public string StoppedSlot
    {
        get
        {
            return _stoppedSlot;
        }
        set
        {
            _stoppedSlot = value;
        }
    }
    public bool RowStopped
    {
        get
        {
            return _rowStopped;
        }
        set
        {
            _rowStopped = value;
        }
    }
    private void Awake()
    {
        _randomizeData = new List<RandomizeData>();
        CreateTheList();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            _randomizeData.Clear();
            CreateTheList();
        }
    }
    private void CreateTheList()
    {
        for (int i = 0; i < _diamondPercent; i++)
        {
            _randomizeData.Add(new RandomizeData("Diamond"));
        }
        for (int i = 0; i < _crownPercent; i++)
        {
            _randomizeData.Add(new RandomizeData("Crown"));
        }
        for (int i = 0; i < _melonPercent; i++)
        {
            _randomizeData.Add(new RandomizeData("Melon"));
        }
        for (int i = 0; i < _barPercent; i++)
        {
            _randomizeData.Add(new RandomizeData("Bar"));
        }
        for (int i = 0; i < _sevenPercent; i++)
        {
            _randomizeData.Add(new RandomizeData("Seven"));
        }
        for (int i = 0; i < _cherryPercent; i++)
        {
            _randomizeData.Add(new RandomizeData("Cherry"));
        }
        for (int i = 0; i < _lemonPercent; i++)
        {
            _randomizeData.Add(new RandomizeData("Lemon"));
        }
    }
    private void Start()
    {
        _rowStopped = true;
        SpinManager.HandlePulled += StartRotating;
    }
    private void StartRotating()
    {
        _stoppedSlot = "";
        StartCoroutine("Rotate");
    }
    private IEnumerator Rotate()
    {
        _rowStopped = false;
        _timeInterval = 0.0025f;

        for (int i = 0; i < 30; i++)
        {
            if (transform.position.y <= -3.5f)
            {
                transform.position = new Vector2(transform.position.x, 1.75f);
            }

            transform.position = new Vector2(transform.position.x, transform.position.y - 0.25f);
            yield return new WaitForSeconds(_timeInterval);
        }
        _randomValue = Random.Range(60, 100);
        switch (_randomValue % 3)
        {
            case 1:
                _randomValue += 2;
                break;
            case 2:
                _randomValue += 1;
                break;
        }

        for (int i = 0; i < _randomValue; i++)
        {
            if (transform.position.y <= _minimumRange)
            {
                transform.position = new Vector2(transform.position.x, _maximumRange);
            }

            transform.position = new Vector2(transform.position.x, transform.position.y - 0.25f);

            if (i > Mathf.RoundToInt(_randomValue * 0.25f))
            {
                _timeInterval = 0.005f;
            }
            if (i > Mathf.RoundToInt(_randomValue * 0.5f))
            {
                _timeInterval = 0.01f;
            }
            if (i > Mathf.RoundToInt(_randomValue * 0.75f))
            {
                _timeInterval = 0.02f;
            }
            if (i > Mathf.RoundToInt(_randomValue * 0.95f))
            {
                _timeInterval = 0.05f;
            }

            yield return new WaitForSeconds(_timeInterval);
        }

        if (transform.position.y == -3.5f)
        {
            _stoppedSlot = "Diamond";
        }
        else if (transform.position.y == -2.75f)
        {
            _stoppedSlot = "Crown";
        }
        else if (transform.position.y == -2f)
        {
            _stoppedSlot = "Melon";
        }
        else if (transform.position.y == -1.25f)
        {
            _stoppedSlot = "Bar";
        }
        else if (transform.position.y == -0.5f)
        {
            _stoppedSlot = "Seven";
        }
        else if (transform.position.y == 0.25f)
        {
            _stoppedSlot = "Cherry";
        }
        else if (transform.position.y == 1f)
        {
            _stoppedSlot = "Lemon";
        }
        else if (transform.position.y == 1.25f)
        {
            _stoppedSlot = "Diamond";
        }

        _rowStopped = true;
        Debug.Log("<color=red> " + _stoppedSlot + "</color>");
        NewMethod();
    }
    private void OnDestroy()
    {
        SpinManager.HandlePulled -= StartRotating;
    }
    private void NewMethod()
    {
        if (_methodSelected == 1)
        {
            var random = Random.Range(0, _randomizeData.Count);
            switch (_randomizeData[random].name)
            {
                case "Diamond": transform.position = new Vector2(transform.position.x, _randomizeData[random].pozition); _stoppedSlot = _randomizeData[random].name; break;
                case "Crown": transform.position = new Vector2(transform.position.x, _randomizeData[random].pozition); _stoppedSlot = _randomizeData[random].name; break;
                case "Melon": transform.position = new Vector2(transform.position.x, _randomizeData[random].pozition); _stoppedSlot = _randomizeData[random].name; break;
                case "Bar": transform.position = new Vector2(transform.position.x, _randomizeData[random].pozition); _stoppedSlot = _randomizeData[random].name; break;
                case "Seven": transform.position = new Vector2(transform.position.x, _randomizeData[random].pozition); _stoppedSlot = _randomizeData[random].name; break;
                case "Cherry": transform.position = new Vector2(transform.position.x, _randomizeData[random].pozition); _stoppedSlot = _randomizeData[random].name; break;
                case "Lemon": transform.position = new Vector2(transform.position.x, _randomizeData[random].pozition); _stoppedSlot = _randomizeData[random].name; break;
            }
            FindObjectOfType<SpinLevels>().UpdateSlider(1, _randomizeData[random].name);
            Debug.Log("<color=green>  Random: " + random + " " + _randomizeData[random].name + "</color>");
        }
    }
}

[System.Serializable]
public class RandomizeData
{
    public string name = default;
    public float pozition = default;
    public RandomizeData(string name)
    {
        if (name == "Diamond")
        {
            this.name = "Diamond";
            this.pozition = -3.5f;
        }
        else if (name == "Crown")
        {
            this.name = "Crown";
            this.pozition = -2.75f;
        }
        else if (name == "Melon")
        {
            this.name = "Melon";
            this.pozition = -2f;
        }
        else if (name == "Bar")
        {
            this.name = "Bar";
            this.pozition = -1.25f;
        }
        else if (name == "Seven")
        {
            this.name = "Seven";
            this.pozition = -0.5f;
        }
        else if (name == "Cherry")
        {
            this.name = "Cherry";
            this.pozition = 0.25f;
        }
        else if (name == "Lemon")
        {
            this.name = "Lemon";
            this.pozition = 1f;
        }
    }
}