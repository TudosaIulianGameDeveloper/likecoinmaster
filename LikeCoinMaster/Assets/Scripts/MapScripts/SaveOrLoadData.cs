﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveOrLoadData : MonoBehaviour
{

    [SerializeField]
    private MapManager _mapManagerData = default;

    private string jsonData = "";
    private string _path = "";

    private void Awake()
    {
        _path = Application.persistentDataPath + "/WizzCoin.json";
    }
    public void SaveGame()
    {
        jsonData = JsonUtility.ToJson(_mapManagerData.MapManagerStatus);
        File.WriteAllText(_path, jsonData);
    }
    private void LoadGame()
    {
        if (File.Exists(_path))
        {
            var loadData = File.ReadAllText(_path);
            MapManagerData playerData = JsonUtility.FromJson<MapManagerData>(loadData);
            _mapManagerData.MapManagerStatus = playerData;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveGame();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadGame();
        }
    }
}
