﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using TMPro;
using UnityEngine.UI;

public class FacebookScript : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _friendList = default;
    [SerializeField]
    private Button _loginButton = default;
    [SerializeField]
    private Button _logoutButton = default;
    [SerializeField]
    private Button _gameRequestButton = default;
    [SerializeField]
    private Button _shareGameButton = default;
    [SerializeField]
    private Button _inviteButton = default;
    [SerializeField]
    private Button _getFriendsList = default;
    [SerializeField]
    private Image _profilePicture = default;
    [Header("CurrentPlayer")]
    [SerializeField]
    private LoginSceneManager _loginManager = default;

    private DataBaseUI _dataBaseUI = default;
    // Start is called before the first frame update
    void Start()
    {
        if (_loginButton != null)
        {
            _loginButton.onClick.AddListener(() => FacebookLogin());
        }
        if (_logoutButton != null)
        {
            _logoutButton.onClick.AddListener(() => FacebookLogout());
        }
        if (_gameRequestButton != null)
        {
            _gameRequestButton.onClick.AddListener(() => FacebookGameRequest());
        }
        if (_shareGameButton != null)
        {
            _shareGameButton.onClick.AddListener(() => FacebookShare());
        }
        if (_inviteButton != null)
        {
            _inviteButton.onClick.AddListener(() => FacebookUserData());
        }
        if (_getFriendsList != null)
        {
            _getFriendsList.onClick.AddListener(() => GetFriendsPlayingThisGame());
        }
        if (_dataBaseUI == null)
        {
            _dataBaseUI = FindObjectOfType<DataBaseUI>();
        }
        _dataBaseUI.LoadPlayers();
    }


    private void Awake()
    {
        InitFacebook();
    }
    private void InitFacebook()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    Debug.LogError("Couldn't init");
                }
            },
            isGameShown =>
            {
                if (!isGameShown)
                {
                    Time.timeScale = 0;
                }
                else
                {
                    Time.timeScale = 1;
                }
            });
        }
        else
        {
            FB.ActivateApp();
        }
    }

    #region Login/Logout
    public void FacebookLogin()
    {
        var permisions = new List<string>()
        {
            "public_profile",
            "email",
            "user_friends"
        };
        FB.LogInWithReadPermissions(permisions, AuthCallBack);
        // AuthCallBack(permisions, AuthCallBack);

    }
    private void AuthCallBack(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            Debug.Log(aToken.UserId);
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }

            if (_dataBaseUI.CheckIfTheUserIsInTheDataBase(aToken.UserId))
            {
                _dataBaseUI.ExtractData(aToken.UserId);
                _loginManager.IsANewUser = false;
            }
            else
            {
                _loginManager.CurrentPlayerData.Data.ID = aToken.UserId;
                _loginManager.IsANewUser = true;
            }
            ExtractNameFrom();
            FacebookUserData();
        }
    }
    public bool IsLoggedIn()
    {
        AccessToken accesToken = AccessToken.CurrentAccessToken;
        return accesToken != null;
    }
    public void FacebookLogout()
    {
        FB.LogOut();
        _loginManager.ActivateMainLoginPanels(MainPanelsNames.LoginSection);
    }
    #endregion

    public void FacebookShare()
    {
        FB.ShareLink(new System.Uri("http://apollo.eed.usv.ro/~tudosa.iulian/"), "Check it out",
            "Good programming tutorial lol",
            new System.Uri("http://apollo.eed.usv.ro/~tudosa.iulian/school/1.jpg"));
    }
    #region Inviting
    public void FacebookGameRequest()
    {
        FB.AppRequest("Hey! Come and play this game!", title: "LordPyro tutorial");
    }

    public void FacebookUserData()
    {
        GetName();
        FB.API("me/picture?type=square&height=128&width=128", HttpMethod.GET, GetPicture);
    }
    public void GetFriendsPlayingThisGame()
    {
        string query = "/me/friends";
        FB.API(query, HttpMethod.GET, result =>
         {
             var dictionary = (Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(result.RawResult);
             var friendList = (List<object>)dictionary["data"];
             _friendList.text = string.Empty;
             foreach (var item in friendList)
             {
                 _friendList.text += ((Dictionary<string, object>)item)["name"];
             }
         });
        if (_friendList.text == string.Empty)
        {
            _friendList.text = "None of your friend accept your game GRPG";
        }
    }
    public void GetPicture(IGraphResult result)
    {
        if (result.Texture != null)
        {
            if (_profilePicture != null)
            {
                _profilePicture.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
            }
        }
    }
    public void GetName()
    {
        FB.API("me?fields=name", Facebook.Unity.HttpMethod.GET, delegate (IGraphResult result)
        {
            if (result.ResultDictionary != null)
            {
                foreach (string key in result.ResultDictionary.Keys)
                {
                    Debug.Log(key + " : " + result.ResultDictionary[key].ToString());
                    if (key == "name")
                        _friendList.text = result.ResultDictionary[key].ToString();
                    if (key == "id")
                    {
                        if (FindObjectOfType<EventLiveStatusUI>() != null)
                        {
                            FindObjectOfType<EventLiveStatusUI>().CurrentUserID = result.ResultDictionary[key].ToString();
                        }
                    }
                }
            }
        });
    }
    public void ExtractNameFrom()
    {
        FB.API("me?fields=name", Facebook.Unity.HttpMethod.GET, delegate (IGraphResult result)
        {
            if (result.ResultDictionary != null)
            {
                foreach (string key in result.ResultDictionary.Keys)
                {
                    Debug.Log(key + " : " + result.ResultDictionary[key].ToString());
                    if (key == "name")
                    {
                        _loginManager.CurrentPlayerData.Data.NAME = result.ResultDictionary[key].ToString();
                        _loginManager.CurrentPlayerData.Data.name = result.ResultDictionary[key].ToString();
                    }
                }
            }
        });
    }
    #endregion
}
