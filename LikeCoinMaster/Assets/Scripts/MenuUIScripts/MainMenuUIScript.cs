﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUIScript : MonoBehaviour
{

    [SerializeField]
    private Text _moneyText = default;
    [SerializeField]
    private Text _diamondsText = default;
    [SerializeField]
    private Button _menuButton = default;
    [SerializeField]
    private Button _cardCollectionButton = default;
    [SerializeField]
    private MenuGameUI _menuGameUIData = default;
    [SerializeField]
    private PlayerDataUI _playerDataUI = default;

    // Start is called before the first frame update
    void Start()
    {
        _menuButton.onClick.AddListener(() =>  _menuGameUIData.GetComponent<Animator>().SetTrigger("OpenMenu"));
        _cardCollectionButton.onClick.AddListener(() => _menuGameUIData.ActivateUIPanel(EnumData.MenuPanelsName.CardCollectionPanel));        
    }

    void Update()
    {
        _moneyText.text = _playerDataUI.OfflineData.COINS.ToString();
        _diamondsText.text = _playerDataUI.OfflineData.DIAMONDS.ToString();
    }
}
