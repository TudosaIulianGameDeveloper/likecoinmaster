﻿using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneNames
{
    LogoScene,
    LoginScene,
    MainGameScene
}

public class LoadSceneManager : MonoBehaviour
{
    private void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Scene");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
    public void LoadScene(SceneNames sceneName)
    {
        switch (sceneName)
        {
            case SceneNames.LogoScene: SceneManager.LoadScene(0);break;
            case SceneNames.LoginScene: SceneManager.LoadScene(1);break;
            case SceneNames.MainGameScene: SceneManager.LoadScene(2); break;
        }
    }

    public void LoadLogoScene()
    {
        LoadScene(SceneNames.LogoScene);
    }
    public void LoadLoginScene()
    {
        LoadScene(SceneNames.LoginScene);
    }
    public void LoadMainGameScene()
    {
        LoadScene(SceneNames.MainGameScene);
    }
}
