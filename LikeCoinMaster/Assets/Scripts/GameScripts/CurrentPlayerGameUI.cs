﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Facebook.Unity;

public class CurrentPlayerGameUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _idUI = default;
    [SerializeField]
    private TextMeshProUGUI _nameUI = default;
    [SerializeField]
    private TextMeshProUGUI _shieldsUI = default;
    [SerializeField]
    private TextMeshProUGUI _starsUI = default;
    [SerializeField]
    private TextMeshProUGUI _coinsUI = default;
    [SerializeField]
    private TextMeshProUGUI _sideUI = default;
    [SerializeField]
    private Image _profileImageUI = default;

    private PlayerDataUI _playerDataUI = default;

    private void Awake()
    {
        if (_playerDataUI == null)
        {
            _playerDataUI = FindObjectOfType<PlayerDataUI>();
        }
    }
    private void Start()
    {
        _idUI.text = _playerDataUI.Data.ID;
        _nameUI.text = _playerDataUI.Data.NAME;
        _shieldsUI.text = _playerDataUI.Data.SHIELDS;
        _starsUI.text = _playerDataUI.Data.STARS;
        _coinsUI.text = _playerDataUI.Data.COINS;
        _sideUI.text = _playerDataUI.Data.SIDE;

        FB.API("me/picture?type=square&height=128&width=128", HttpMethod.GET, GetPicture);

    }
    public void GetPicture(IGraphResult result)
    {
        if (result.Texture != null)
        {
            _profileImageUI.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
        }
    }

}
