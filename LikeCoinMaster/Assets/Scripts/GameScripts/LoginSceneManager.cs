﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public enum MainPanelsNames
{
    LoginSection,
    FacebookSection,
    FirebaseSection,
    ChooseYourSideSection
}
public class LoginSceneManager : MonoBehaviour
{
    [Header("LoginSectionUI")]
    [SerializeField]
    private GameObject _loginButtonsSection = default;
    [SerializeField]
    private Button _facebookButtonLogin = default;
    [SerializeField]
    private Button _firebaseButtonLogin = default;
    [SerializeField]
    private Button _exitAppButtonLogin = default;

    [Header("FacebookSectionUI")]
    [SerializeField]
    private GameObject _facebookButtonsSection = default;
    [SerializeField]
    private Button _fbButtonLogin = default;
    [SerializeField]
    private Button _playTheGame = default;
    [SerializeField]
    private Button _backAndLogout = default;
    [SerializeField]
    private TextMeshProUGUI _nickname = default;
    [SerializeField]
    private Image _profileImage = default;

    [Header("FirebaseSectionUI")]
    [SerializeField]
    private GameObject _firebaseButtonsSection = default;
    [SerializeField]
    private Button _normalButtonLogin = default;
    [SerializeField]
    private Button _normalButtonLogout = default;
    [SerializeField]
    private TMP_InputField _emailInput = default;
    [SerializeField]
    private TMP_InputField _passInput = default;

    [Header("FinalPlayerData")]
    [SerializeField]
    private GameObject _chooseYourSidePanel = default;
    [SerializeField]
    private Button _facebookPlayButton = default;
    [SerializeField]
    private Button _firebasePlayButton = default;
    [SerializeField]
    private Button _lightSideButton = default;
    [SerializeField]
    private Button _darkSideButton = default;


    [Header("CurrentPlayerData")]
    [SerializeField]
    private PlayerDataUI _currentPlayer = default;

    private FacebookScript _facebookManager = default;
    private AuthController _firebaseManager = default;
    [SerializeField]
    private bool _isANewUser = default;
    

    public bool IsANewUser
    {
        get
        {
            return _isANewUser;
        }
        set
        {
            _isANewUser = value;
        }
    }

    public PlayerDataUI CurrentPlayerData
    {
        get
        {
            return _currentPlayer;
        }
        set
        {
            _currentPlayer = value;
        }
    }


    private void Awake()
    {
        _facebookManager = FindObjectOfType<FacebookScript>();
        _firebaseManager = FindObjectOfType<AuthController>();
        _currentPlayer.Data.ID = "";
    }
    private void Update()
    {

    }
    private void Start()
    {
        LoginSectionUIButtonsAction();
        ChooseYourSidePanel();
    }

    private void LoginSectionUIButtonsAction()
    {
        _facebookButtonLogin.onClick.AddListener(() => ActivateMainLoginPanels(MainPanelsNames.FacebookSection));
        _firebaseButtonLogin.onClick.AddListener(() =>
        {
            _firebaseManager.ActivateFireBaseButtonsUI();
            ActivateMainLoginPanels(MainPanelsNames.FirebaseSection);
        });
        _exitAppButtonLogin.onClick.AddListener(() => Application.Quit());
    }
    private void ChooseYourSidePanel()
    {
        _facebookPlayButton.onClick.AddListener(() => IsANewUserVerification());
        _firebasePlayButton.onClick.AddListener(() => IsANewUserVerification());

        _lightSideButton.onClick.AddListener(() => UpdateCurrentPlayerWithHisSide(true));
        _darkSideButton.onClick.AddListener(() => UpdateCurrentPlayerWithHisSide(false));
    }
    private void IsANewUserVerification()
    {
        if (_currentPlayer.Data.ID != "")
        {
            if (IsANewUser)
            {
                ActivateMainLoginPanels(MainPanelsNames.ChooseYourSideSection);
            }
            else
            {
                FindObjectOfType<LoadSceneManager>().LoadScene(SceneNames.MainGameScene);
            }
        }
        else
        {
            Debug.Log("You are not logged");
        }
    }
    private void UpdateCurrentPlayerWithHisSide(bool lightOrDarkSide)
    {
        if (lightOrDarkSide)
        {
            _currentPlayer.Data.SIDE = "Light";
        }
        else
        {
            _currentPlayer.Data.SIDE = "Dark";
        }
        _currentPlayer.Data.COINS = "0";
        _currentPlayer.Data.SHIELDS = "0";
        _currentPlayer.Data.STARS = "0";

        FindObjectOfType<DataBaseUI>().AddPlayer(_currentPlayer.Data);
        FindObjectOfType<LoadSceneManager>().LoadScene(SceneNames.MainGameScene);
    }

    public void ActivateMainLoginPanels(MainPanelsNames name)
    {
        switch (name)
        {
            case MainPanelsNames.LoginSection: PanelsSectionStatus(true, false, false, false); break;
            case MainPanelsNames.FacebookSection: PanelsSectionStatus(false, true, false, false); if (_facebookManager.IsLoggedIn()) { _facebookManager.FacebookUserData(); }; break;
            case MainPanelsNames.FirebaseSection: PanelsSectionStatus(false, false, true, false); break;
            case MainPanelsNames.ChooseYourSideSection: PanelsSectionStatus(false, false, false, true); break;
        }
    }

    private void PanelsSectionStatus(bool login, bool facebook, bool firebase, bool side)
    {
        _loginButtonsSection.SetActive(login);
        _facebookButtonsSection.SetActive(facebook);
        _firebaseButtonsSection.SetActive(firebase);
        _chooseYourSidePanel.SetActive(side);
    }
}
