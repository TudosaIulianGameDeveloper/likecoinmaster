﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventManager : MonoBehaviour
{
    [SerializeField]
    private SceneNames _sceneLoading = default;

    public void LoadNextScene()
    {
        FindObjectOfType<LoadSceneManager>().LoadScene(_sceneLoading);
    }
}
